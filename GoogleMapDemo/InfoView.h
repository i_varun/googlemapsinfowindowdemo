//
//  InfoView.h
//  GoogleMapDemo
//
//  Created by Varun on 7/14/16.
//  Copyright © 2016 Varun. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface InfoView : UIView
@property (strong, nonatomic) IBOutlet UILabel *mainTitle;
@property (strong, nonatomic) IBOutlet UIImageView *imgView;

@end
