//
//  ViewController.m
//  GoogleMapDemo
//
//  Created by Varun on 4/21/16.
//  Copyright © 2016 Varun. All rights reserved.
//

#import "ViewController.h"
@import GoogleMaps;
#import <CoreLocation/CoreLocation.h>
#import "InfoView.h"
#import <UIKit/UIKit.h>
@interface ViewController () <GMSMapViewDelegate>

@end

@implementation ViewController {

    CLLocationDegrees lat;
    CLLocationDegrees lon;
    GMSMapView *googleMap;
    
    NSMutableArray *arrLat;
    NSMutableArray *arrLong;
    NSMutableArray *arrImages;
    NSMutableArray *arrPlaceName;

}

- (void)viewDidLoad {
    [super viewDidLoad];

    arrLat = [[NSMutableArray alloc]initWithObjects:@"10.442403",@"33.743575",@"34.167569",@"32.236877",nil];
    arrLong = [[NSMutableArray alloc]initWithObjects:@"76.080186",@"78.614990",@"77.583747",@"77.191068",nil];
    arrImages = [[NSMutableArray alloc]initWithObjects:@"1.jpg",@"2.jpg",@"3.jpg",@"4.png",nil];
    
    arrPlaceName = [[NSMutableArray alloc]initWithObjects:@"Kerala",@"Jamnu",@"Manali",@"Leh",nil];
    
    self.locationManager = [[CLLocationManager alloc]init];
    self.locationManager.delegate = self;

    
    if ([self.locationManager respondsToSelector:@selector
         (requestWhenInUseAuthorization)]) {
        [self.locationManager requestWhenInUseAuthorization];
    }
    [self.locationManager startUpdatingLocation];
    
    lat = self.locationManager.location.coordinate.latitude;
    lon = self.locationManager.location.coordinate.longitude;
        [self loadGoogleMap];

    // Do any additional setup after loading the view, typically from a nib.
}

- (void)loadGoogleMap{

    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:23.604318
                                                            longitude:72.972709
                                                                 zoom:6];
    googleMap = [GMSMapView mapWithFrame:CGRectZero camera:camera];
    googleMap.myLocationEnabled = YES;
    self.view = googleMap;
    googleMap.delegate = self;

    for(int i = 0; i < [arrLat count]; i++)
    {

        CLLocationCoordinate2D position = { [[arrLat objectAtIndex:i]doubleValue], [[arrLong objectAtIndex:i]doubleValue] };
        GMSMarker *marker = [GMSMarker markerWithPosition:position];
        marker.title = [NSString stringWithFormat:@"%@", [arrPlaceName objectAtIndex:i]];
        marker.appearAnimation = YES;
        marker.flat = YES;
        marker.snippet = @"";
        marker.map = googleMap;
    }
}

- (UIView *)mapView:(GMSMapView *)mapView markerInfoWindow:(GMSMarker *)marker {
    InfoView *view =  [[[NSBundle mainBundle] loadNibNamed:@"InfoView" owner:self options:nil] objectAtIndex:0];
    NSUInteger position = [arrPlaceName indexOfObject:marker.title];
    
    view.mainTitle.text = [arrPlaceName objectAtIndex:position];
    view.imgView.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@",[arrImages objectAtIndex:position]]];
    return view;
}

- (void)mapView:(GMSMapView *)mapView didTapInfoWindowOfMarker:(GMSMarker *)marker{
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
