//
//  InfoView.m
//  GoogleMapDemo
//
//  Created by Varun on 7/14/16.
//  Copyright © 2016 Varun. All rights reserved.
//

#import "InfoView.h"

@implementation InfoView


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    self.layer.cornerRadius = 10;
    self.clipsToBounds = YES;
    
    self.imgView.clipsToBounds = YES;
    self.imgView.layer.cornerRadius = 25;
    self.imgView.layer.borderWidth = 2;
    self.imgView.layer.borderColor = [UIColor grayColor].CGColor;
}


@end
