//
//  ViewController.h
//  GoogleMapDemo
//
//  Created by Varun on 4/21/16.
//  Copyright © 2016 Varun. All rights reserved.
//

#import <UIKit/UIKit.h>
@import GoogleMaps;
@interface ViewController : UIViewController<CLLocationManagerDelegate>

//@property (weak, nonatomic) IBOutlet GMSMapView *googleMap;
@property (nonatomic,strong) CLLocationManager *locationManager;

@end

